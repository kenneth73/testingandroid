package cat.itb.testingexample;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.View;

import androidx.test.core.app.ActivityScenario;
import androidx.test.espresso.Espresso;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withResourceName;
import static androidx.test.espresso.matcher.ViewMatchers.withText;


@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityScenarioRule<MainActivity> publicActivityScenarioRule = new ActivityScenarioRule<>(MainActivity.class);

    // ACTIVITY 2 //
    @Test
    public void elementsOnMainActivityAreDisplayedCorrectly() {
        //a la vista mainactivity comprovamos que se esta mostrando
        onView(withId(R.id.textViewTitle)).check(matches(isDisplayed()));
        onView(withId(R.id.btnChangeText)).check(matches(isDisplayed()));
    }

    @Test
    public void elementsOnMainactivityTextAreCorrect() {
        onView(withId(R.id.textViewTitle)).check(matches(withText("Hello World!")));
        onView(withId(R.id.btnChangeText)).check(matches(withText("Change text")));
    }

    @Test
    public void elementsOnMainactivityCheckNextBtnIsClicableAndWhenPressChangeTextView() {
        // comproba que es pot fer click y despres fa click
        onView(withId(R.id.btnChangeText)).check(matches(isClickable())).perform(click());
        // comproba que el text del boto es "Back"
        onView(withId(R.id.textViewTitle)).check(matches(withText("Back")));
    }

    // ACTIVITY 3 //
    @Test
    public void loginFormBehaviour(){

        onView(withId(R.id.username)).perform(replaceText(getResourceString(R.string.USER_TO_BE_TYPED)));
        onView(withId(R.id.password)).perform(replaceText(getResourceString(R.string.PASS_TO_BE_TYPED)));
        Espresso.closeSoftKeyboard();
        //onView(withId(R.id.buttonNext)).perform(click()); // para pasar la funcion de abajo
        //onView(withId(R.id.buttonNext)).check(matches(withText("Registrat"))); // no pasa el test porque al clicar cambiamos de activity y no detecta el View

    }


    // ACTIVITY 4 //
    @Test
    public void comproveNavigationMainActivityToFirstActivityWithButton() {
        onView(withId(R.id.buttonNext)).check(matches(isClickable())).perform(click());
        onView(withId(R.id.firstActivity)).check(matches(isDisplayed()));
    }


    @Test
    public void comproveNavigationFirstActivityToMainActivityWithButton() {
        onView(withId(R.id.buttonNext)).check(matches(isClickable())).perform(click());
        onView(withId(R.id.firstActivity)).check(matches(isDisplayed()));
        onView(withId(R.id.btnFirstActivity)).check(matches(isClickable())).perform(click());
        onView(withId(R.id.mainActivity)).check(matches(isDisplayed()));
    }


    // ACTIVITY 5 //
    @Test
    public void comproveNameAndPasswordAndGoMainToFirstActivityAndCheckTextViewTitleAndGoToMainActivityAndComproveEmptyEditText() {
        //introduir el nom i password al editText
        onView(withId(R.id.username)).perform(replaceText(getResourceString(R.string.USER_TO_BE_TYPED)));
        onView(withId(R.id.password)).perform(replaceText(getResourceString(R.string.PASS_TO_BE_TYPED)));
        Espresso.closeSoftKeyboard();
        //anar cap al FirstActivity
        onView(withId(R.id.buttonNext)).check(matches(isClickable())).perform(click());
        //comprobar que estem en el second activity
        onView(withId(R.id.firstActivity)).check(matches(isDisplayed()));
        //comprobar que el text del firstactivity es "Welcome back username"
        onView(withId(R.id.textViewFirstActivity)).check(matches(withText("Welcome back kenneth")));
        //anar enrere
        onView(withId(R.id.btnFirstActivity)).check(matches(isClickable())).perform(click());
        //comprobar que estem al main activity
        onView(withId(R.id.mainActivity)).check(matches(isDisplayed()));
        //comprobar que els camps estan buits
        onView(withId(R.id.username)).check(matches(withText("")));
        onView(withId(R.id.password)).check(matches(withText("")));
    }




    private String getResourceString(int id) {
        Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        return targetContext.getResources().getString(id);
    }



}
