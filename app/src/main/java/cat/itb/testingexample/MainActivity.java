package cat.itb.testingexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

//https://docs.google.com/document/d/16q5AHiwggB8iziFAu51Cty09pNS7C03LEBYXOPuRygs/edit

public class MainActivity extends AppCompatActivity {

    TextView textViewTitle;
    Button buttonChangeText;

    TextView username, password;
    Button bntNext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewTitle = findViewById(R.id.textViewTitle);
        buttonChangeText = findViewById(R.id.btnChangeText);

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);

        bntNext = findViewById(R.id.buttonNext);

        bntNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bntNext.setText("Registrat");
                //TODO: pasarle el nombre a la siguiente actividad con un bundle
                // y mostrarlo en el titulo de la actividad
                Intent intent = new Intent(MainActivity.this, FirstActivity.class);
                intent.putExtra("username", username.getText().toString());
                intent.putExtra("password", password.getText().toString());
                username.setText("");
                password.setText("");
                startActivity(intent);
            }

        });

        buttonChangeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textViewTitle.setText("Back");
            }
        });
    }
}