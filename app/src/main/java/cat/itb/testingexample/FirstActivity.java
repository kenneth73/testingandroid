package cat.itb.testingexample;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class FirstActivity extends AppCompatActivity {

    Button bntGoBack;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_activity);

        textView = findViewById(R.id.textViewFirstActivity);
        bntGoBack = findViewById(R.id.btnFirstActivity);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            textView.setText("Welcome back " + bundle.get("username"));

        }


        bntGoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FirstActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }
}
